package net.pl3x.bukkit.christmastrees.listener;

import net.pl3x.bukkit.christmastrees.Logger;
import net.pl3x.bukkit.christmastrees.TreeManager;
import net.pl3x.bukkit.christmastrees.configuration.Lang;
import net.pl3x.bukkit.christmastrees.tree.Tree;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

import java.util.List;
import java.util.stream.Collectors;

public class TreeListener implements Listener {
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event) {
        Block block = event.getBlock();

        if (!block.getType().equals(Material.LOG) && !block.getType().equals(Material.LEAVES)) {
            return; // not a log/leaves block
        }

        Tree tree = TreeManager.getTree(block.getLocation());
        if (tree == null) {
            return; // not a christmas tree
        }

        Player player = event.getPlayer();
        if (!player.hasPermission("can.remove.christmastrees")) {
            event.setCancelled(true);
            Lang.send(player, Lang.CANNOT_REMOVE_TREE);
            return; // no permission to remove christmas trees
        }

        if (tree.isProtected(player)) {
            event.setCancelled(true);
            Lang.send(player, Lang.TREE_PROTECTED);
            return; // player doesn't have rights to break this tree
        }

        // Remove tree
        String locationString = tree.getLocationString();
        TreeManager.remove(tree);
        Logger.debug("Removed tree from: " + locationString);
        Lang.send(player, Lang.TREE_REMOVED);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void protectFromPistonPush(BlockPistonExtendEvent event) {
        Block piston = event.getBlock();
        for (Block block : event.getBlocks()) {
            if (TreeManager.getTree(block.getLocation()) != null) {
                event.setCancelled(true);
                piston.setType(Material.AIR);
                piston.getWorld().createExplosion(piston.getLocation(), 0F);
                return;
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void protectFromPistonPull(BlockPistonRetractEvent event) {
        Block piston = event.getBlock();
        for (Block block : event.getBlocks()) {
            if (TreeManager.getTree(block.getLocation()) != null) {
                event.setCancelled(true);
                piston.setType(Material.AIR);
                piston.getWorld().createExplosion(piston.getLocation(), 0F);
                return;
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onBlockExplode(BlockExplodeEvent event) {
        if (TreeManager.getTree(event.getBlock().getLocation()) == null) {
            return; // not a tree
        }
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onEntityExplode(EntityExplodeEvent event) {
        List<Block> remove = event.blockList().stream()
                .filter(block -> TreeManager.getTree(block.getLocation()) != null)
                .collect(Collectors.toList());
        event.blockList().removeAll(remove);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onEntityChangeBlock(EntityChangeBlockEvent event) {
        if (TreeManager.getTree(event.getBlock().getLocation()) == null) {
            return; // not a tree
        }
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onBlockPhysics(BlockPhysicsEvent event) {
        if (TreeManager.getTreeByStar(event.getBlock().getLocation()) != null) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onBlockBurn(BlockBurnEvent event) {
        if (TreeManager.getTree(event.getBlock().getLocation()) == null) {
            return; // not a tree block
        }

        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onBlockIgnite(BlockIgniteEvent event) {
        if (isTouchingTree(event.getBlock())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onLeafDecay(LeavesDecayEvent event) {
        if (TreeManager.getTree(event.getBlock().getLocation()) == null) {
            return; // not a tree block
        }

        event.setCancelled(true);
    }

    private boolean isTouchingTree(Block block) {
        return TreeManager.getTree(block.getLocation()) != null ||
                TreeManager.getTreeByStar(block.getLocation()) != null ||
                TreeManager.getTree(block.getRelative(BlockFace.UP).getLocation()) != null ||
                TreeManager.getTree(block.getRelative(BlockFace.DOWN).getLocation()) != null ||
                TreeManager.getTree(block.getRelative(BlockFace.EAST).getLocation()) != null ||
                TreeManager.getTree(block.getRelative(BlockFace.WEST).getLocation()) != null ||
                TreeManager.getTree(block.getRelative(BlockFace.NORTH).getLocation()) != null ||
                TreeManager.getTree(block.getRelative(BlockFace.SOUTH).getLocation()) != null;
    }
}
