package net.pl3x.bukkit.christmastrees;

public class ParticleColor {
    private final float red;
    private final float green;
    private final float blue;

    public ParticleColor(int red, int green, int blue) {
        this.red = (red == 0 ? 0.0001F : red) / 255;
        this.green = (green == 0 ? 0.0001F : green) / 255;
        this.blue = (blue == 0 ? 0.0001F : blue) / 255;
    }

    public float getRed() {
        return red;
    }

    public float getGreen() {
        return green;
    }

    public float getBlue() {
        return blue;
    }
}
