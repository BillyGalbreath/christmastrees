package net.pl3x.bukkit.christmastrees;

import net.pl3x.bukkit.christmastrees.command.CmdChristmasTree;
import net.pl3x.bukkit.christmastrees.configuration.Config;
import net.pl3x.bukkit.christmastrees.configuration.Lang;
import net.pl3x.bukkit.christmastrees.listener.TreeListener;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public class ChristmasTrees extends JavaPlugin {
    @Override
    public void onEnable() {
        Config.reload();
        Lang.reload();

        if (!Bukkit.getPluginManager().isPluginEnabled("ProtocolLib")) {
            Logger.error("Dependency plugin ProtocolLib not found!");
            Logger.error("Please install dependency plugin.");
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }

        getCommand("christmastree").setExecutor(new CmdChristmasTree(this));

        Bukkit.getPluginManager().registerEvents(new TreeListener(), this);

        TreeManager.loadTrees();

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        TreeManager.unloadAll();

        Logger.info(getName() + " disabled.");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&4" + getName() + " is disabled. See console log for more information."));
        return true;
    }

    public static ChristmasTrees getPlugin() {
        return ChristmasTrees.getPlugin(ChristmasTrees.class);
    }
}
