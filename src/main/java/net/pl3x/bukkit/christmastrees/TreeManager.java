package net.pl3x.bukkit.christmastrees;

import net.pl3x.bukkit.christmastrees.configuration.TreeConfig;
import net.pl3x.bukkit.christmastrees.tree.LargeTree;
import net.pl3x.bukkit.christmastrees.tree.MediumTree;
import net.pl3x.bukkit.christmastrees.tree.SmallTree;
import net.pl3x.bukkit.christmastrees.tree.Tree;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TreeManager {
    private static final Set<Tree> trees = new HashSet<>();

    public static void add(Tree tree) {
        TreeConfig treeConfig = TreeConfig.getConfig();
        List<String> list = treeConfig.getStringList("trees");
        String locationString = tree.getLocationString();
        list.add(locationString + "," + tree.getSize());
        treeConfig.set("trees", list);
        treeConfig.save();

        load(tree);
    }

    public static void remove(Tree tree) {
        TreeConfig treeConfig = TreeConfig.getConfig();
        List<String> list = treeConfig.getStringList("trees");
        String locationString = tree.getLocationString();
        list.remove(locationString); // leave this to get rid of any legacy trees
        list.remove(locationString + "," + tree.getSize());
        treeConfig.set("trees", list);
        treeConfig.save();

        tree.remove();
    }

    private static void load(Tree tree) {
        trees.add(tree);

        Logger.debug("Loaded tree at: " + tree.getLocationString());
    }

    public static void unloadAll() {
        trees.forEach(Tree::unload);
        trees.clear();
    }

    public static Tree getTree(Location loc) {
        for (Tree tree : trees) {
            if (tree.contains(loc)) {
                return tree;
            }
        }
        return null;
    }

    public static Tree getTreeByStar(Location loc) {
        for (Tree tree : trees) {
            if (tree.getStar().equals(loc)) {
                return tree;
            }
        }
        return null;
    }

    public static void loadTrees() {
        TreeConfig treeConfig = TreeConfig.getConfig();
        List<String> list = treeConfig.getStringList("trees");
        for (String entry : list) {
            String[] str = entry.split(",");

            World world = Bukkit.getWorld(str[0]);
            if (world == null) {
                Logger.warn("World not found: " + str[0] + " (" + entry + ")");
                continue;
            }

            int x, y, z, size;
            try {
                x = Integer.parseInt(str[1]);
                y = Integer.parseInt(str[2]);
                z = Integer.parseInt(str[3]);
            } catch (NumberFormatException e) {
                Logger.warn("Invalid location: " + str[1] + "," + str[2] + "," + str[3] + " (" + entry + ")");
                continue;
            }

            try {
                size = Integer.parseInt(str[4]);
            } catch (NumberFormatException e) {
                Logger.warn("Invalid size: " + str[4] + " (" + entry + ")");
                continue;
            } catch (ArrayIndexOutOfBoundsException e) {
                Logger.debug("No size specified. Defaulting to small." + " (" + entry + ")");
                size = 0;
            }

            try {
                Tree tree;
                Location loc = new Location(world, x, y, z);
                switch (size) {
                    default:
                    case 0:
                        tree = new SmallTree(loc, false);
                        break;
                    case 1:
                        tree = new MediumTree(loc, false);
                        break;
                    case 2:
                        tree = new LargeTree(loc, false);
                        break;
                }
                load(tree);
            } catch (IllegalStateException e) {
                Logger.warn("Error loading christmas tree at " + entry);
            }
        }
    }
}
