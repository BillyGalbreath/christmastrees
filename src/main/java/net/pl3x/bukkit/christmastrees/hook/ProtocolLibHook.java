package net.pl3x.bukkit.christmastrees.hook;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;

public class ProtocolLibHook {
    private static ProtocolManager protocolManager;

    public static ProtocolManager getManager() {
        if (protocolManager == null) {
            protocolManager = ProtocolLibrary.getProtocolManager();
        }
        return protocolManager;
    }
}
