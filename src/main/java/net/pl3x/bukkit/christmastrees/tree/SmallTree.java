package net.pl3x.bukkit.christmastrees.tree;

import org.bukkit.Location;

public class SmallTree extends Tree {
    public SmallTree(Location ground) {
        this(ground, true);
    }

    public SmallTree(Location ground, boolean checkAir) {
        super(ground, checkAir);
    }

    @Override
    public int getSize() {
        return 0;
    }

    @Override
    protected void populateBlocks() {
        // logs
        logs.add(getGround().add(0, 1, 0));
        logs.add(getGround().add(0, 2, 0)); // layer 1
        logs.add(getGround().add(0, 3, 0)); // layer 2

        // layer 1
        leaves.add(getGround().add(1, 2, 0));
        leaves.add(getGround().add(1, 2, 1));
        leaves.add(getGround().add(1, 2, -1));
        leaves.add(getGround().add(0, 2, 1));
        leaves.add(getGround().add(0, 2, -1));
        leaves.add(getGround().add(-1, 2, 0));
        leaves.add(getGround().add(-1, 2, 1));
        leaves.add(getGround().add(-1, 2, -1));

        // layer 2
        leaves.add(getGround().add(1, 3, 0));
        leaves.add(getGround().add(-1, 3, 0));
        leaves.add(getGround().add(0, 3, 1));
        leaves.add(getGround().add(0, 3, -1));

        // layer 3
        leaves.add(getGround().add(0, 4, 0));

        // star
        star = getGround().add(0, 5, 0);
    }

    @Override
    protected void setParticleLocations() {
        // layer 1
        particles.add(getGround().add(-0.5, 2.5, -1));
        particles.add(getGround().add(0.5, 2.5, -1));
        particles.add(getGround().add(1.5, 2.5, -1));
        particles.add(getGround().add(-1, 2.5, -0.5));
        particles.add(getGround().add(-1, 2.5, 0.5));
        particles.add(getGround().add(-1, 2.5, 1.5));
        particles.add(getGround().add(2, 2.5, -0.5));
        particles.add(getGround().add(2, 2.5, 0.5));
        particles.add(getGround().add(2, 2.5, 1.5));
        particles.add(getGround().add(-0.5, 2.5, 2));
        particles.add(getGround().add(0.5, 2.5, 2));
        particles.add(getGround().add(1.5, 2.5, 2));

        // layer 2
        particles.add(getGround().add(0, 3.5, -0.5));
        particles.add(getGround().add(0.5, 3.5, -1));
        particles.add(getGround().add(1, 3.5, -0.5));
        particles.add(getGround().add(1.5, 3.5, 0));
        particles.add(getGround().add(2, 3.5, 0.5));
        particles.add(getGround().add(1.5, 3.5, 1));
        particles.add(getGround().add(1, 3.5, 1.5));
        particles.add(getGround().add(0.5, 3.5, 2));
        particles.add(getGround().add(-0.5, 3.5, 1.5));
        particles.add(getGround().add(-0.5, 3.5, 1));
        particles.add(getGround().add(-1, 3.5, 0.5));
        particles.add(getGround().add(-0.5, 3.5, 0));

        // top
        particles.add(getGround().add(0.5, 4.5, 0));
        particles.add(getGround().add(0.5, 4.5, 1));
        particles.add(getGround().add(0, 4.5, 0.5));
        particles.add(getGround().add(1, 4.5, 1));
    }
}
