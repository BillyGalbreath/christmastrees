package net.pl3x.bukkit.christmastrees.tree;

import org.bukkit.Location;

public class MediumTree extends Tree {
    public MediumTree(Location ground) {
        this(ground, true);
    }

    public MediumTree(Location ground, boolean checkAir) {
        super(ground, checkAir);
    }

    @Override
    public int getSize() {
        return 1;
    }

    @Override
    protected void populateBlocks() {
        // logs
        logs.add(getGround().add(0, 1, 0));
        logs.add(getGround().add(0, 2, 0)); // layer 1
        logs.add(getGround().add(0, 3, 0)); // layer 2
        logs.add(getGround().add(0, 4, 0)); // layer 3
        logs.add(getGround().add(0, 5, 0)); // layer 4
        logs.add(getGround().add(0, 6, 0)); // layer 5
        logs.add(getGround().add(0, 7, 0)); // layer 6

        // layer 1
        leaves.add(getGround().add(1, 2, 0));
        leaves.add(getGround().add(2, 2, 0));
        leaves.add(getGround().add(3, 2, 0));
        leaves.add(getGround().add(1, 2, 1));
        leaves.add(getGround().add(2, 2, 1));
        leaves.add(getGround().add(3, 2, 1));
        leaves.add(getGround().add(1, 2, 2));
        leaves.add(getGround().add(2, 2, 2));
        leaves.add(getGround().add(1, 2, 3));
        leaves.add(getGround().add(1, 2, -1));
        leaves.add(getGround().add(2, 2, -1));
        leaves.add(getGround().add(3, 2, -1));
        leaves.add(getGround().add(1, 2, -2));
        leaves.add(getGround().add(2, 2, -2));
        leaves.add(getGround().add(1, 2, -3));
        leaves.add(getGround().add(0, 2, -1));
        leaves.add(getGround().add(0, 2, -2));
        leaves.add(getGround().add(0, 2, -3));
        leaves.add(getGround().add(0, 2, 1));
        leaves.add(getGround().add(0, 2, 2));
        leaves.add(getGround().add(0, 2, 3));
        leaves.add(getGround().add(-1, 2, 0));
        leaves.add(getGround().add(-2, 2, 0));
        leaves.add(getGround().add(-3, 2, 0));
        leaves.add(getGround().add(-1, 2, 1));
        leaves.add(getGround().add(-2, 2, 1));
        leaves.add(getGround().add(-3, 2, 1));
        leaves.add(getGround().add(-1, 2, 2));
        leaves.add(getGround().add(-2, 2, 2));
        leaves.add(getGround().add(-1, 2, 3));
        leaves.add(getGround().add(-1, 2, -1));
        leaves.add(getGround().add(-2, 2, -1));
        leaves.add(getGround().add(-3, 2, -1));
        leaves.add(getGround().add(-1, 2, -2));
        leaves.add(getGround().add(-2, 2, -2));
        leaves.add(getGround().add(-1, 2, -3));

        // layer 2
        leaves.add(getGround().add(1, 3, 0));
        leaves.add(getGround().add(2, 3, 0));
        leaves.add(getGround().add(3, 3, 0));
        leaves.add(getGround().add(1, 3, 1));
        leaves.add(getGround().add(2, 3, 1));
        leaves.add(getGround().add(1, 3, 2));
        leaves.add(getGround().add(1, 3, -1));
        leaves.add(getGround().add(2, 3, -1));
        leaves.add(getGround().add(1, 3, -2));
        leaves.add(getGround().add(0, 3, 1));
        leaves.add(getGround().add(0, 3, 2));
        leaves.add(getGround().add(0, 3, 3));
        leaves.add(getGround().add(0, 3, -1));
        leaves.add(getGround().add(0, 3, -2));
        leaves.add(getGround().add(0, 3, -3));
        leaves.add(getGround().add(-1, 3, 0));
        leaves.add(getGround().add(-2, 3, 0));
        leaves.add(getGround().add(-3, 3, 0));
        leaves.add(getGround().add(-1, 3, 1));
        leaves.add(getGround().add(-2, 3, 1));
        leaves.add(getGround().add(-1, 3, 2));
        leaves.add(getGround().add(-1, 3, -1));
        leaves.add(getGround().add(-2, 3, -1));
        leaves.add(getGround().add(-1, 3, -2));

        // layer 3
        leaves.add(getGround().add(1, 4, 0));
        leaves.add(getGround().add(2, 4, 0));
        leaves.add(getGround().add(1, 4, 1));
        leaves.add(getGround().add(2, 4, 1));
        leaves.add(getGround().add(1, 4, 2));
        leaves.add(getGround().add(1, 4, -1));
        leaves.add(getGround().add(2, 4, -1));
        leaves.add(getGround().add(1, 4, -2));
        leaves.add(getGround().add(0, 4, 1));
        leaves.add(getGround().add(0, 4, 2));
        leaves.add(getGround().add(0, 4, -1));
        leaves.add(getGround().add(0, 4, -2));
        leaves.add(getGround().add(-1, 4, 0));
        leaves.add(getGround().add(-2, 4, 0));
        leaves.add(getGround().add(-1, 4, 1));
        leaves.add(getGround().add(-2, 4, 1));
        leaves.add(getGround().add(-1, 4, 2));
        leaves.add(getGround().add(-1, 4, -1));
        leaves.add(getGround().add(-2, 4, -1));
        leaves.add(getGround().add(-1, 4, -2));

        // layer 4
        leaves.add(getGround().add(1, 5, 0));
        leaves.add(getGround().add(2, 5, 0));
        leaves.add(getGround().add(1, 5, 1));
        leaves.add(getGround().add(1, 5, -1));
        leaves.add(getGround().add(0, 5, 1));
        leaves.add(getGround().add(0, 5, 2));
        leaves.add(getGround().add(0, 5, -1));
        leaves.add(getGround().add(0, 5, -2));
        leaves.add(getGround().add(-1, 5, 0));
        leaves.add(getGround().add(-2, 5, 0));
        leaves.add(getGround().add(-1, 5, 1));
        leaves.add(getGround().add(-1, 5, -1));

        // layer 5
        leaves.add(getGround().add(1, 6, 0));
        leaves.add(getGround().add(1, 6, 1));
        leaves.add(getGround().add(1, 6, -1));
        leaves.add(getGround().add(0, 6, 1));
        leaves.add(getGround().add(0, 6, -1));
        leaves.add(getGround().add(-1, 6, 0));
        leaves.add(getGround().add(-1, 6, 1));
        leaves.add(getGround().add(-1, 6, -1));

        // layer 6
        leaves.add(getGround().add(1, 7, 0));
        leaves.add(getGround().add(-1, 7, 0));
        leaves.add(getGround().add(0, 7, 1));
        leaves.add(getGround().add(0, 7, -1));

        // top
        leaves.add(getGround().add(0, 8, 0));

        // star
        star = getGround().add(0, 9, 0);
    }

    @Override
    protected void setParticleLocations() {
        //layer 1
        particles.add(getGround().add(-0.5, 2.5, -3));
        particles.add(getGround().add(0.5, 2.5, -3));
        particles.add(getGround().add(1.5, 2.5, -3));
        particles.add(getGround().add(2, 2.5, -2.5));
        particles.add(getGround().add(2.5, 2.5, -2));
        particles.add(getGround().add(3, 2.5, -1.5));
        particles.add(getGround().add(3.5, 2.5, -1));
        particles.add(getGround().add(4, 2.5, -0.5));
        particles.add(getGround().add(4, 2.5, 0.5));
        particles.add(getGround().add(4, 2.5, 1.5));
        particles.add(getGround().add(3.5, 2.5, 2));
        particles.add(getGround().add(3, 2.5, 2.5));
        particles.add(getGround().add(2.5, 2.5, 3));
        particles.add(getGround().add(2, 2.5, 3.5));
        particles.add(getGround().add(1.5, 2.5, 4));
        particles.add(getGround().add(0.5, 2.5, 4));
        particles.add(getGround().add(-0.5, 2.5, 4));
        particles.add(getGround().add(-1, 2.5, 3.5));
        particles.add(getGround().add(-1.5, 2.5, 3));
        particles.add(getGround().add(-2, 2.5, 2.5));
        particles.add(getGround().add(-2.5, 2.5, 2));
        particles.add(getGround().add(-3, 2.5, 1.5));
        particles.add(getGround().add(-3, 2.5, 0.5));
        particles.add(getGround().add(-3, 2.5, -0.5));
        particles.add(getGround().add(-2.5, 2.5, -1));
        particles.add(getGround().add(-2, 2.5, -1.5));
        particles.add(getGround().add(-1.5, 2.5, -2));
        particles.add(getGround().add(-1, 2.5, -2.5));

        // layer 2
        particles.add(getGround().add(0.5, 3.5, -3));
        particles.add(getGround().add(1, 3.5, -2.5));
        particles.add(getGround().add(1.5, 3.5, -2));
        particles.add(getGround().add(2, 3.5, -1.5));
        particles.add(getGround().add(2.5, 3.5, -1));
        particles.add(getGround().add(3, 3.5, -0.5));
        particles.add(getGround().add(3.5, 3.5, 0));
        particles.add(getGround().add(4, 3.5, 0.5));
        particles.add(getGround().add(3.5, 3.5, 1));
        particles.add(getGround().add(3, 3.5, 1.5));
        particles.add(getGround().add(2.5, 3.5, 2));
        particles.add(getGround().add(2, 3.5, 2.5));
        particles.add(getGround().add(1.5, 3.5, 3));
        particles.add(getGround().add(1, 3.5, 3.5));
        particles.add(getGround().add(0.5, 3.5, 4));
        particles.add(getGround().add(0, 3.5, 3.5));
        particles.add(getGround().add(-0.5, 3.5, 3));
        particles.add(getGround().add(-1, 3.5, 2.5));
        particles.add(getGround().add(-1.5, 3.5, 2));
        particles.add(getGround().add(-2, 3.5, 1.5));
        particles.add(getGround().add(-2.5, 3.5, 1));
        particles.add(getGround().add(-3, 3.5, 0.5));
        particles.add(getGround().add(-2.5, 3.5, 0));
        particles.add(getGround().add(-2, 3.5, -0.5));
        particles.add(getGround().add(-1.5, 3.5, -1));
        particles.add(getGround().add(-1, 3.5, -1.5));
        particles.add(getGround().add(-0.5, 3.5, -2));
        particles.add(getGround().add(0, 3.5, -2.5));

        // layer 3
        particles.add(getGround().add(-0.5, 4.5, -2));
        particles.add(getGround().add(0.5, 4.5, -2));
        particles.add(getGround().add(1.5, 4.5, -2));
        particles.add(getGround().add(2, 4.5, -1.5));
        particles.add(getGround().add(2.5, 4.5, -1));
        particles.add(getGround().add(3, 4.5, -0.5));
        particles.add(getGround().add(3, 4.5, 0.5));
        particles.add(getGround().add(3, 4.5, 1.5));
        particles.add(getGround().add(2.5, 4.5, 2));
        particles.add(getGround().add(2, 4.5, 2.5));
        particles.add(getGround().add(1.5, 4.5, 3));
        particles.add(getGround().add(0.5, 4.5, 3));
        particles.add(getGround().add(-0.5, 4.5, 3));
        particles.add(getGround().add(-1, 4.5, 2.5));
        particles.add(getGround().add(-1.5, 4.5, 2));
        particles.add(getGround().add(-2, 4.5, 1.5));
        particles.add(getGround().add(-2, 4.5, 0.5));
        particles.add(getGround().add(-2, 4.5, -0.5));
        particles.add(getGround().add(-1.5, 4.5, -1));
        particles.add(getGround().add(-1, 4.5, -1.5));

        // layer 4
        particles.add(getGround().add(0.5, 5.5, -2));
        particles.add(getGround().add(1, 5.5, -1.5));
        particles.add(getGround().add(1.5, 5.5, -1));
        particles.add(getGround().add(2, 5.5, -0.5));
        particles.add(getGround().add(2.5, 5.5, 0));
        particles.add(getGround().add(3, 5.5, 0.5));
        particles.add(getGround().add(2.5, 5.5, 1));
        particles.add(getGround().add(2, 5.5, 1.5));
        particles.add(getGround().add(1.5, 5.5, 2));
        particles.add(getGround().add(1, 5.5, 2.5));
        particles.add(getGround().add(0.5, 5.5, 3));
        particles.add(getGround().add(0, 5.5, 2.5));
        particles.add(getGround().add(-0.5, 5.5, 2));
        particles.add(getGround().add(-1, 5.5, 1.5));
        particles.add(getGround().add(-1.5, 5.5, 1));
        particles.add(getGround().add(-2, 5.5, 0.5));
        particles.add(getGround().add(-1.5, 5.5, 0));
        particles.add(getGround().add(-1, 5.5, -0.5));
        particles.add(getGround().add(-0.5, 5.5, -1));
        particles.add(getGround().add(0, 5.5, -1.5));

        // layer 5
        particles.add(getGround().add(-0.5, 6.5, -1));
        particles.add(getGround().add(0.5, 6.5, -1));
        particles.add(getGround().add(1.5, 6.5, -1));
        particles.add(getGround().add(-1, 6.5, -0.5));
        particles.add(getGround().add(-1, 6.5, 0.5));
        particles.add(getGround().add(-1, 6.5, 1.5));
        particles.add(getGround().add(2, 6.5, -0.5));
        particles.add(getGround().add(2, 6.5, 0.5));
        particles.add(getGround().add(2, 6.5, 1.5));
        particles.add(getGround().add(-0.5, 6.5, 2));
        particles.add(getGround().add(0.5, 6.5, 2));
        particles.add(getGround().add(1.5, 6.5, 2));

        // layer 6
        particles.add(getGround().add(0, 7.5, -0.5));
        particles.add(getGround().add(0.5, 7.5, -1));
        particles.add(getGround().add(1, 7.5, -0.5));
        particles.add(getGround().add(1.5, 7.5, 0));
        particles.add(getGround().add(2, 7.5, 0.5));
        particles.add(getGround().add(1.5, 7.5, 1));
        particles.add(getGround().add(1, 7.5, 1.5));
        particles.add(getGround().add(0.5, 7.5, 2));
        particles.add(getGround().add(-0.5, 7.5, 1.5));
        particles.add(getGround().add(-0.5, 7.5, 1));
        particles.add(getGround().add(-1, 7.5, 0.5));
        particles.add(getGround().add(-0.5, 7.5, 0));

        // top
        particles.add(getGround().add(0.5, 8.5, 0));
        particles.add(getGround().add(0.5, 8.5, 1));
        particles.add(getGround().add(0, 8.5, 0.5));
        particles.add(getGround().add(1, 8.5, 1));
    }
}
