package net.pl3x.bukkit.christmastrees.tree;

import net.pl3x.bukkit.christmastrees.ChristmasTrees;
import net.pl3x.bukkit.christmastrees.Logger;
import net.pl3x.bukkit.christmastrees.configuration.Config;
import net.pl3x.bukkit.christmastrees.configuration.Lang;
import net.pl3x.bukkit.christmastrees.task.ParticleTask;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;

import java.util.HashSet;
import java.util.Set;

public abstract class Tree {
    private Location ground = null;
    Location star = null;
    Set<Location> logs = new HashSet<>();
    Set<Location> leaves = new HashSet<>();
    Set<Location> particles = new HashSet<>();
    private ParticleTask particleTask = null;

    Tree(Location ground, boolean checkAir) {
        this.ground = ground;

        populateBlocks();

        boolean failed = false;
        if (checkAir) {
            if (!canBuildTree()) {
                failed = true;
            }
        } else {
            if (!isProperTree()) {
                failed = true;
            }
        }

        if (failed) {
            unload();
            throw new IllegalStateException(Lang.CANNOT_BUILD_HERE);
        }

        setParticleLocations();

        particleTask = new ParticleTask(this);
        particleTask.runTaskTimer(ChristmasTrees.getPlugin(ChristmasTrees.class), 20L, Config.LIGHTS_DELAY);
    }

    protected abstract void populateBlocks();

    protected abstract void setParticleLocations();

    private boolean canBuildTree() {
        // check ground is grass/dirt
        if (ground == null) {
            Logger.debug("No ground");
            return false; // no ground
        }

        Chunk chunk = ground.getChunk();
        if (!chunk.isLoaded()) {
            chunk.load();
        }

        Block groundBlock = ground.getBlock();
        if (!groundBlock.getType().equals(Material.GRASS) && !groundBlock.getType().equals(Material.DIRT)) {
            Logger.debug("Not grass/dirt");
            return false; // ground not grass/dirt
        }

        if (!star.getBlock().getType().equals(Material.AIR)) {
            Logger.debug("Not air: " + star);
            return false; // cannot place star
        }

        // check log positions
        for (Location loc : logs) {
            if (!loc.getBlock().getType().equals(Material.AIR)) {
                Logger.debug("Not air: " + loc);
                return false; // cannot place log
            }
        }

        // check leaves positions
        for (Location loc : leaves) {
            if (!loc.getBlock().getType().equals(Material.AIR)) {
                Logger.debug("Not air: " + loc);
                return false; // cannot place leaves
            }
        }

        // can build tree here
        return true;
    }

    private boolean isProperTree() {
        // check ground is grass/dirt
        if (ground == null) {
            Logger.debug("No ground");
            return false; // no ground
        }

        Chunk chunk = ground.getChunk();
        if (!chunk.isLoaded()) {
            chunk.load();
        }

        Block groundBlock = ground.getBlock();
        if (!groundBlock.getType().equals(Material.GRASS) && !groundBlock.getType().equals(Material.DIRT)) {
            Logger.debug("Not grass/dirt");
            return false; // ground not grass/dirt
        }

        // check log positions
        for (Location loc : logs) {
            Block block = loc.getBlock();
            //noinspection deprecation
            byte data = block.getData();
            if (!block.getType().equals(Material.LOG) || data != 1) {
                Logger.debug("Not log: " + loc + " " + block.getType() + ":" + data);
                return false; // cannot place log
            }
        }

        // check leaves positions
        for (Location loc : leaves) {
            Block block = loc.getBlock();
            //noinspection deprecation
            byte data = block.getData();
            if (!(block.getType().equals(Material.LEAVES) && (data == 1 || data == 5 || data == 9 || data == 13))) {
                Logger.debug("Not leaves: " + loc + " " + block.getType() + ":" + data);
                return false; // cannot place leaves
            }
        }

        // do not check for star (for backwards compatibility)

        return true;
    }

    public boolean isProtected(Player player) {
        PluginManager pm = Bukkit.getPluginManager();
        Block groundBlock = ground.getBlock();
        BlockState air = groundBlock.getRelative(BlockFace.UP).getState();

        for (Location loc : logs) {
            Block block = loc.getBlock();
            BlockPlaceEvent event = new BlockPlaceEvent(block, air, groundBlock, new ItemStack(Material.AIR), player, true, EquipmentSlot.HAND);
            pm.callEvent(event);
            if (event.isCancelled() || !event.canBuild()) {
                Logger.debug("Log place cancelled: " + loc);
                return true;
            }
        }

        for (Location loc : leaves) {
            Block block = loc.getBlock();
            BlockPlaceEvent event = new BlockPlaceEvent(block, air, groundBlock, new ItemStack(Material.AIR), player, true, EquipmentSlot.HAND);
            pm.callEvent(event);
            if (event.isCancelled() || !event.canBuild()) {
                Logger.debug("Leaves place cancelled: " + loc);
                return true;
            }
        }

        Block block = star.getBlock();
        BlockPlaceEvent event = new BlockPlaceEvent(block, air, groundBlock, new ItemStack(Material.AIR), player, true, EquipmentSlot.HAND);
        pm.callEvent(event);
        if (event.isCancelled() || !event.canBuild()) {
            Logger.debug("Star place cancelled: " + star);
            return true;
        }

        return false;
    }

    public void placeBlocks() {
        for (Location loc : logs) {
            Logger.debug("Placing log at " + loc);
            Block log = loc.getBlock();
            log.setType(Material.LOG);
            log.setData((byte) 1);
        }
        for (Location loc : leaves) {
            Logger.debug("Placing leaves at " + loc);
            Block leaves = loc.getBlock();
            leaves.setType(Material.LEAVES);
            leaves.setData((byte) 5);
        }

        Block starBlock = star.getBlock();
        starBlock.setType(Config.STAR_MATERIAL);
        starBlock.setData((byte) Config.STAR_DATA);
    }

    public String getLocationString() {
        return ground.getWorld().getName() + "," + ground.getBlockX() + "," + ground.getBlockY() + "," + ground.getBlockZ();
    }

    public Location getGround() {
        return ground.clone();
    }

    public Location getStar() {
        return star.clone();
    }

    public Set<Location> getParticleLocations() {
        return particles;
    }

    public abstract int getSize();

    public void unload() {
        if (particleTask != null) {
            particleTask.cancel();
        }

        particleTask = null;
        particles = null;
        ground = null;
        logs = null;
        leaves = null;
    }

    public void remove() {
        for (Location loc : logs) {
            loc.getBlock().setType(Material.AIR);
        }
        for (Location loc : leaves) {
            loc.getBlock().setType(Material.AIR);
        }

        star.getBlock().setType(Material.AIR);

        unload();
    }

    public boolean contains(Location location) {
        return !(logs == null || leaves == null) && (contains(location, logs) || contains(location, leaves));
    }

    private boolean contains(Location loc, Set<Location> locs) {
        for (Location loc1 : locs) {
            if (loc1 == null) {
                continue;
            }
            if (!loc1.getWorld().getName().equals(loc.getWorld().getName())) {

                continue;
            }
            if (loc1.getBlockX() != loc.getBlockX()) {
                continue;
            }
            if (loc1.getBlockY() != loc.getBlockY()) {
                continue;
            }
            if (loc1.getBlockZ() != loc.getBlockZ()) {
                continue;
            }
            return true;
        }
        return false;
    }
}
