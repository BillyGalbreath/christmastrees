package net.pl3x.bukkit.christmastrees.task;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.EnumWrappers;
import net.pl3x.bukkit.christmastrees.ChristmasTrees;
import net.pl3x.bukkit.christmastrees.ParticleColor;
import net.pl3x.bukkit.christmastrees.configuration.Config;
import net.pl3x.bukkit.christmastrees.hook.ProtocolLibHook;
import net.pl3x.bukkit.christmastrees.tree.Tree;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class ParticleTask extends BukkitRunnable {
    private final Tree tree;
    private final Random rand = new Random();

    public ParticleTask(Tree tree) {
        this.tree = tree;
    }

    @Override
    public void run() {
        Location base = tree.getGround();
        Set<Player> players = new HashSet<>();

        for (Player player : Bukkit.getOnlinePlayers()) {
            Location pLoc = player.getLocation();
            if (!pLoc.getWorld().equals(base.getWorld())) {
                continue;
            }
            if (player.getLocation().distanceSquared(base) < 2500) {
                players.add(player);
            }
        }

        if (players.isEmpty()) {
            return;
        }

        for (Location pos : tree.getParticleLocations()) {
            ParticleColor color = new ParticleColor(rand.nextInt(256), rand.nextInt(256), rand.nextInt(256));
            if (Config.SPARKLE) {
                if (rand.nextInt(100) <= Config.SPARKLE_DENSITY) {
                    Bukkit.getScheduler().runTaskLater(ChristmasTrees.getPlugin(), () -> sendPacket(players, pos, color), rand.nextInt(Config.LIGHTS_DELAY));
                }
            } else {
                sendPacket(players, pos, color);
            }
        }
    }

    private void sendPacket(Set<Player> players, Location loc, ParticleColor color) {
        ProtocolManager manager = ProtocolLibHook.getManager();

        PacketContainer packet = manager.createPacket(PacketType.Play.Server.WORLD_PARTICLES);

        packet.getParticles().write(0, EnumWrappers.Particle.REDSTONE);
        packet.getIntegers().write(0, 0);      // amount
        packet.getBooleans().write(0, true);   // long distance
        packet.getFloat().
                write(0, (float) loc.getX()).  // x position
                write(1, (float) loc.getY()).  // y position
                write(2, (float) loc.getZ()).  // z position
                write(3, color.getRed()).      // offsetX (red)
                write(4, color.getGreen()).    // offsetY (green)
                write(5, color.getBlue()).     // offsetZ (blue)
                write(6, 1F);                  // data (speed)

        for (Player player : players) {
            try {
                manager.sendServerPacket(player, packet);
            } catch (InvocationTargetException ignore) {
            }
        }
    }
}
