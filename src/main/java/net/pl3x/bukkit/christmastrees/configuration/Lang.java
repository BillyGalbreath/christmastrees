package net.pl3x.bukkit.christmastrees.configuration;

import net.pl3x.bukkit.christmastrees.ChristmasTrees;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public class Lang {
    public static String COMMAND_NO_PERMISSION = "&4You do not have permission for this command!";
    public static String COMMAND_NO_PERMISSION_SIZE = "&4You do not have permission for {size} trees!";
    public static String PLAYER_COMMAND = "&4Player only command!";
    public static String NO_TARGET_BLOCK = "&4No target block!";
    public static String INVALID_SIZE = "Invalid size given! Acceptable sizes: small, medium, large";
    public static String AREA_PROTECTED = "&4This area is protected!";
    public static String TREE_PROTECTED = "&4This tree is protected!";
    public static String CANNOT_BUILD_HERE = "&4Cannot build tree here!";
    public static String CANNOT_REMOVE_TREE = "&4You are not allowed to remove this tree!";
    public static String TREE_ADDED = "&dChristmas tree successfully spawned.";
    public static String TREE_REMOVED = "&dChristmas tree successfully removed.";
    public static String VERSION = "&d{plugin} v{version}.";
    public static String RELOAD = "&d{plugin} v{version} reloaded.";

    public static void reload() {
        ChristmasTrees plugin = ChristmasTrees.getPlugin();
        String langFile = Config.LANGUAGE_FILE;
        File configFile = new File(plugin.getDataFolder(), langFile);
        if (!configFile.exists()) {
            plugin.saveResource(Config.LANGUAGE_FILE, false);
        }
        FileConfiguration config = YamlConfiguration.loadConfiguration(configFile);

        COMMAND_NO_PERMISSION = config.getString("command-no-permission", "&4You do not have permission for this command!");
        COMMAND_NO_PERMISSION_SIZE = config.getString("command-no-permission-size", "&4You do not have permission for {size} trees!");
        PLAYER_COMMAND = config.getString("player-command", "&4Player only command!");
        NO_TARGET_BLOCK = config.getString("no-target-block", "&4No target block!");
        INVALID_SIZE = config.getString("invalid-size", "Invalid size given! Acceptable sizes: small, medium, large");
        AREA_PROTECTED = config.getString("area-protected", "&4This area is protected!");
        TREE_PROTECTED = config.getString("tree-protected", "&4This tree is protected!");
        CANNOT_BUILD_HERE = config.getString("cannot-build-here", "&4Cannot build tree here!");
        CANNOT_REMOVE_TREE = config.getString("cannot-remove-tree", "&4You are not allowed to remove this tree!");
        TREE_ADDED = config.getString("tree-added", "&dChristmas tree successfully spawned.");
        TREE_REMOVED = config.getString("tree-removed", "&dChristmas tree successfully removed.");
        VERSION = config.getString("version", "&d{plugin} v{version}.");
        RELOAD = config.getString("reload", "&d{plugin} v{version} reloaded.");
    }

    public static void send(CommandSender recipient, String message) {
        if (message == null) {
            return; // do not send blank messages
        }
        message = ChatColor.translateAlternateColorCodes('&', message);
        if (ChatColor.stripColor(message).isEmpty()) {
            return; // do not send blank messages
        }

        for (String part : message.split("\n")) {
            recipient.sendMessage(part);
        }
    }
}
