package net.pl3x.bukkit.christmastrees.configuration;

import net.pl3x.bukkit.christmastrees.ChristmasTrees;
import net.pl3x.bukkit.christmastrees.Logger;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;

public class Config {
    public static boolean COLOR_LOGS = true;
    public static boolean DEBUG_MODE = false;
    public static String LANGUAGE_FILE = "lang-en.yml";
    public static Material STAR_MATERIAL = Material.YELLOW_FLOWER;
    public static int STAR_DATA = 0;
    public static int LIGHTS_DELAY = 10;
    public static boolean SPARKLE = false;
    public static int SPARKLE_DENSITY = 25;

    public static void reload() {
        ChristmasTrees plugin = ChristmasTrees.getPlugin();
        plugin.saveDefaultConfig();
        plugin.reloadConfig();
        FileConfiguration config = plugin.getConfig();

        COLOR_LOGS = config.getBoolean("color-logs", true);
        DEBUG_MODE = config.getBoolean("debug-mode", false);
        LANGUAGE_FILE = config.getString("language-file", "lang-en.yml");
        SPARKLE = config.getBoolean("sparkle", false);
        SPARKLE_DENSITY = config.getInt("sparkle-density", 25);
        LIGHTS_DELAY = config.getInt("lights-delay", 10);
        STAR_DATA = config.getInt("star-data", 0);
        try {
            STAR_MATERIAL = Material.matchMaterial(config.getString("star-block", "YELLOW_FLOWER"));
        } catch (IllegalArgumentException e) {
            Logger.error("Star material not recognized. Defaulting to YELLOW_FLOWER.");
            STAR_MATERIAL = Material.YELLOW_FLOWER;
            STAR_DATA = 0;
        }
    }
}
