package net.pl3x.bukkit.christmastrees.configuration;

import net.pl3x.bukkit.christmastrees.ChristmasTrees;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public class TreeConfig extends YamlConfiguration {
    private static TreeConfig config;

    public static TreeConfig getConfig() {
        if (config == null) {
            config = new TreeConfig();
        }
        return config;
    }

    public static void reload() {
        config = null;
        getConfig();
    }

    private final File file;
    private final Object saveLock = new Object();

    private TreeConfig() {
        super();
        file = new File(ChristmasTrees.getPlugin(ChristmasTrees.class).getDataFolder(), "trees.yml");
        load();
    }

    private void load() {
        synchronized (saveLock) {
            try {
                this.load(file);
            } catch (Exception ignore) {
            }
        }
    }

    public void save() {
        synchronized (saveLock) {
            try {
                save(file);
            } catch (Exception ignore) {
            }
        }
    }
}
