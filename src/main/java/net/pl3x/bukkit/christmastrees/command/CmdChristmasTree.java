package net.pl3x.bukkit.christmastrees.command;

import net.pl3x.bukkit.christmastrees.ChristmasTrees;
import net.pl3x.bukkit.christmastrees.Logger;
import net.pl3x.bukkit.christmastrees.TreeManager;
import net.pl3x.bukkit.christmastrees.configuration.Config;
import net.pl3x.bukkit.christmastrees.configuration.Lang;
import net.pl3x.bukkit.christmastrees.configuration.TreeConfig;
import net.pl3x.bukkit.christmastrees.tree.LargeTree;
import net.pl3x.bukkit.christmastrees.tree.MediumTree;
import net.pl3x.bukkit.christmastrees.tree.SmallTree;
import net.pl3x.bukkit.christmastrees.tree.Tree;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class CmdChristmasTree implements TabExecutor {
    private final ChristmasTrees plugin;

    public CmdChristmasTree(ChristmasTrees plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        List<String> list = new ArrayList<>();
        if (args.length == 1) {
            List<String> available = new ArrayList<>();
            if (sender.hasPermission("command.christmastree.small")) {
                available.add("small");
            }
            if (sender.hasPermission("command.christmastree.medium")) {
                available.add("medium");
            }
            if (sender.hasPermission("command.christmastree.large")) {
                available.add("large");
            }
            if (sender.hasPermission("command.christmastree.reload")) {
                available.add("reload");
            }
            String arg = args[0].trim().toLowerCase();
            list.addAll(available.stream()
                    .filter(sub -> sub.startsWith(arg))
                    .collect(Collectors.toList()));
        }
        return list;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            Lang.send(sender, Lang.PLAYER_COMMAND);
            return true;
        }

        if (!sender.hasPermission("command.christmastree.small") &&
                !sender.hasPermission("command.christmastree.medium") &&
                !sender.hasPermission("command.christmastree.large") &&
                !sender.hasPermission("command.christmastree.reload")) {
            Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        int size = 0;
        if (args.length > 0) {
            if (args[0].equalsIgnoreCase("reload")) {
                if (!sender.hasPermission("command.christmastree.reload")) {
                    Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
                    return true;
                }

                TreeManager.unloadAll();

                Config.reload();
                Lang.reload();
                TreeConfig.reload();

                TreeManager.loadTrees();

                Lang.send(sender, Lang.RELOAD
                        .replace("{plugin}", plugin.getName())
                        .replace("{version}", plugin.getDescription().getVersion()));
                return true;
            } else if (args[0].equalsIgnoreCase("small")) {
                if (!sender.hasPermission("command.christmastree.small")) {
                    Lang.send(sender, Lang.COMMAND_NO_PERMISSION_SIZE.replace("{size}", "small"));
                    return true;
                }
                size = 0;
            } else if (args[0].equalsIgnoreCase("medium")) {
                if (!sender.hasPermission("command.christmastree.medium")) {
                    Lang.send(sender, Lang.COMMAND_NO_PERMISSION_SIZE.replace("{size}", "medium"));
                    return true;
                }
                size = 1;
            } else if (args[0].equalsIgnoreCase("large")) {
                if (!sender.hasPermission("command.christmastree.large")) {
                    Lang.send(sender, Lang.COMMAND_NO_PERMISSION_SIZE.replace("{size}", "large"));
                    return true;
                }
                size = 2;
            } else {
                Lang.send(sender, Lang.INVALID_SIZE);
                return true;
            }
        } else {
            if (!sender.hasPermission("command.christmastree.small")) {
                Lang.send(sender, Lang.COMMAND_NO_PERMISSION_SIZE.replace("{size}", "small"));
                return true;
            }
        }

        Player player = (Player) sender;

        Set<Material> transparent = new HashSet<>();
        transparent.add(Material.AIR);
        Block targetBlock = player.getTargetBlock(transparent, 20);

        if (targetBlock == null || targetBlock.getType().equals(Material.AIR)) {
            Lang.send(sender, Lang.NO_TARGET_BLOCK);
            return true;
        }

        Tree tree;
        try {
            switch (size) {
                default:
                case 0:
                    tree = new SmallTree(targetBlock.getLocation());
                    break;
                case 1:
                    tree = new MediumTree(targetBlock.getLocation());
                    break;
                case 2:
                    tree = new LargeTree(targetBlock.getLocation());
                    break;
            }
        } catch (IllegalStateException e) {
            Lang.send(sender, e.getMessage());
            return true;
        }

        if (tree.isProtected(player)) {
            Lang.send(sender, Lang.AREA_PROTECTED);
            tree.unload();
            return true;
        }

        TreeManager.add(tree);

        tree.placeBlocks();

        Logger.debug("Added tree at: " + tree.getLocationString());
        Lang.send(sender, Lang.TREE_ADDED);
        return true;
    }
}
